package reverseminds.sparks.virtualive;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import com.beyondar.android.view.OnClickBeyondarObjectListener;
import com.beyondar.android.world.BeyondarObject;
import java.util.ArrayList;

/**
 * Created by Varun on 15-07-2017.
 */

public class ARActivity extends FragmentActivity {

    private static final int MY_REQUEST_CODE =200;
    com.beyondar.android.fragment.BeyondarFragmentSupport mBeyondarFragment;
    com.beyondar.android.world.World world;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_ar);

        mBeyondarFragment = (com.beyondar.android.fragment.BeyondarFragmentSupport) getSupportFragmentManager().findFragmentById(R.id.beyondarFragment);
        //mBeyondarFragment.setPullCloserDistance(20);
        mBeyondarFragment.setPushAwayDistance(10);
        mBeyondarFragment.setMaxDistanceToRender(1000);
        mBeyondarFragment.setDistanceFactor(10);

        //LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        world = new com.beyondar.android.world.World(this);
        world.setGeoPosition(12.966375, 77.711679);
        world.setDefaultImage(R.drawable.ar);

        com.beyondar.android.world.GeoObject go1 = new com.beyondar.android.world.GeoObject();
        com.beyondar.android.world.GeoObject go2 = new com.beyondar.android.world.GeoObject();
        com.beyondar.android.world.GeoObject go3 = new com.beyondar.android.world.GeoObject();
        com.beyondar.android.world.GeoObject go4 = new com.beyondar.android.world.GeoObject();

        go1.setGeoPosition(12.966562, 77.711606);
        go1.setName("go1");
        go1.setImageResource(R.drawable.go1);
        world.addBeyondarObject(go1);

        go3.setGeoPosition(12.966296, 77.711416);
        go3.setName("go3");
        go3.setImageResource(R.drawable.go3);
        world.addBeyondarObject(go3);

        go2.setGeoPosition(12.966181, 77.711795);
        go2.setName("go2");
        go2.setImageResource(R.drawable.go2);
        world.addBeyondarObject(go2);

        go4.setGeoPosition(12.966464, 77.711903);
        go4.setName("go4");
        go4.setImageResource(R.drawable.go4);
        world.addBeyondarObject(go4);

        world.addBeyondarObject(go1);
        world.addBeyondarObject(go2);
        world.addBeyondarObject(go3);
        world.addBeyondarObject(go4);

        mBeyondarFragment.setWorld(world);
        mBeyondarFragment.setOnClickBeyondarObjectListener(new OnClickBeyondarObjectListener() {
            @Override
            public void onClickBeyondarObject(ArrayList<BeyondarObject> beyondarObjects) {
                for(int i=0; i<1; i++)
                {
                    if(beyondarObjects.get(0).getName().toString().equals("go1"))
                    {
                        Intent in = new Intent(ARActivity.this, Texts.class);
                        startActivity(in);
                    }
                    if(beyondarObjects.get(0).getName().toString().equals("go2"))
                    {
                        Intent in = new Intent(ARActivity.this, Texts.class);
                        startActivity(in);
                    }
                    if(beyondarObjects.get(0).getName().toString().equals("go3"))
                    {
                        Intent in = new Intent(ARActivity.this, VrVideoActivity.class);
                        in.putExtra("video", "flashback");
                        startActivity(in);
                    }
                    if(beyondarObjects.get(0).getName().toString().equals("go4"))
                    {
                        Intent in = new Intent(ARActivity.this, VrVideoActivity.class);
                        in.putExtra("video", "flashback");
                        startActivity(in);
                    }
                }
                //Toast.makeText(getBaseContext(), "Clicked on: " + beyondarObjects.get(0).getName(), Toast.LENGTH_LONG).show();
            }
        });

        final AlertDialog.Builder dialog = new AlertDialog.Builder(ARActivity.this);
        dialog.setTitle("Ar view")
                .setMessage("Move your phone around and click on markers to view related videos and read relevant content.")
                .setCancelable(false)
                .setNegativeButton("OK! Got it!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        com.beyondar.android.util.location.BeyondarLocationManager.disable();
    }

    @Override
    protected void onResume() {
        super.onResume();
        com.beyondar.android.util.location.BeyondarLocationManager.enable();
    }
}
