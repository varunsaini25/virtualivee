package reverseminds.sparks.virtualive;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Varun on 15-07-2017.
 */

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE =200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermission();
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    public void guide(View view)
    {
        Intent i = new Intent(MainActivity.this, VrVideoActivity.class);
        Toast.makeText(getBaseContext(), "       Get a headstart to the \nmuseum tour with Virtual Guide", Toast.LENGTH_LONG).show();
        Toast.makeText(getBaseContext(), "       Get a headstart to the \nmuseum tour with Virtual Guide", Toast.LENGTH_LONG).show();
        i.putExtra("video", "guide");
        startActivity(i);
    }

    public void ar(View view)
    {
        Intent i = new Intent(MainActivity.this, ARActivity.class);
        Toast.makeText(getBaseContext(), "Please wait...", Toast.LENGTH_LONG).show();
        startActivity(i);
    }

    public void flash(View view)
    {
        Intent i = new Intent(MainActivity.this, Flashbacks.class);
        i.putExtra("video", "flashback");
        startActivity(i);
    }

}
