package reverseminds.sparks.virtualive;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by Varun on 15-07-2017.
 */

public class Flashbacks extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flashback);
        final AlertDialog.Builder dialog = new AlertDialog.Builder(Flashbacks.this);
        dialog.setTitle("Flaaaaashback")
                .setMessage("Let history repeat itself.\nBe a part of it !!!")
                .setCancelable(false)
                .setNegativeButton("OK! Got it!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    public void goals(View view)
    {
        Intent i = new Intent(Flashbacks.this, VrVideoActivity.class);
        i.putExtra("video", "flashback");
        startActivity(i);
    }

    public void mathces(View view)
    {
        Intent i = new Intent(Flashbacks.this, VrVideoActivity.class);
        i.putExtra("video", "flashback");
        startActivity(i);
    }

}
